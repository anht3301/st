static const char d_black[]	  = "#1e2127";
static const char d_red[]     = "#e06c75";
static const char d_green[]   = "#98c379";
static const char d_yellow[]  = "#d19a66";
static const char d_blue[]    = "#61afef";
static const char d_magenta[] = "#c678dd";
static const char d_cyan[]    = "#56b6c2";
static const char d_white[]   = "#abb2bf";

static const char b_black[]   = "#5c6370";
static const char b_red[]     = "#e06c75";
static const char b_green[]   = "#98c379";
static const char b_yellow[]  = "#d19a66";
static const char b_blue[]    = "#61afef";
static const char b_magenta[] = "#c678dd";
static const char b_cyan[]    = "#56b6c2";
static const char b_white[]   = "#ffffff";








