static const char bg[]   = "#181818";
static const char fg[]   = "#b9b9b9";

static const char d_black[]	  = "#252525";
static const char d_red[]     = "#ed4a46";
static const char d_green[]   = "#70b433";
static const char d_yellow[]  = "#dbb32d";
static const char d_blue[]    = "#368aeb";
static const char d_magenta[] = "#eb6eb7";
static const char d_cyan[]    = "#3fc5b7";
static const char d_white[]   = "#777777";

static const char b_black[]   = "#3b3b3b";
static const char b_red[]     = "#ff5e56";
static const char b_green[]   = "#83c746";
static const char b_yellow[]  = "#efc541";
static const char b_blue[]    = "#4f9cfe";
static const char b_magenta[] = "#ff81ca";
static const char b_cyan[]    = "#56d8c9";
static const char b_white[]   = "#dedede";
