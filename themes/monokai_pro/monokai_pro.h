static const char d_black[]	  = "#2c2525";
static const char d_red[]     = "#fd6883";
static const char d_green[]   = "#adda78";
static const char d_yellow[]  = "#f9cc6c";
static const char d_blue[]    = "#f38d70";
static const char d_magenta[] = "#a8a9eb";
static const char d_cyan[]    = "#85dacc";
static const char d_white[]   = "#fff1f3";

static const char b_black[]   = "#72696a";
static const char b_red[]     = "#fd6883";
static const char b_green[]   = "#adda78";
static const char b_yellow[]  = "#f9cc6c";
static const char b_blue[]    = "#f38d70";
static const char b_magenta[] = "#a8a9eb";
static const char b_cyan[]    = "#85dacc";
static const char b_white[]   = "#fff1f3";
